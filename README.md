
basic-akka

```bash
curl -kv http://localhost:5000/movies | jq
curl -kv http://localhost:5000/movies -X POST --data "{ \"name\": \"Arrival2\", \"tickets\": [] }" -H "Content-Type: application/json"
curl -kv http://localhost:5000/movies/arrival -X DELETE
```