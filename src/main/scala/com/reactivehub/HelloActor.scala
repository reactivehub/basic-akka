package com.reactivehub

import akka.actor.Actor

class HelloActor extends Actor {

  override def receive: Receive = {

    case SayHello() => println("Hello there !!!")

    case _ => println("I don' understand ...")

  }
}
