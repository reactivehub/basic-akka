package com.reactivehub

import akka.actor.{Actor, Props}
import scala.util.{Failure, Success}

/**
  * Companion object for movie actor with all possible messages
  */
object MovieActor {

  def props() = Props(new MovieActor)

  case class GetMovies()

  case class GetMovie(name: String)

  case class AddMovie(movie: Movie)

  case class DeleteMovie(name: String)

}

/**
  * Actor handling movie objects
  */
class MovieActor extends Actor {

  import MovieActor._

  var movies = Map[String, Movie]()
  movies += ("arrival" -> Movie("Arrival", List(Ticket("18:00", "a10"))))
  movies += ("doctor-strange" -> Movie("Doctor-Strange", List(Ticket("18:00", "a10"))))

  override def receive: Receive = {

    case GetMovies() => sender() ! movies.values

    case GetMovie(name) => sender() ! movies.get(name.toLowerCase())

    case AddMovie(movie) => {
      movies += (movie.name.toLowerCase() -> movie)
      sender() ! Success
    }

    case DeleteMovie(name) => {
      val nameLower = name.toLowerCase()
      val contained = movies.contains(nameLower)
      movies -= nameLower
      if (contained) sender() ! Failure else sender() ! Success
    }
  }
}
