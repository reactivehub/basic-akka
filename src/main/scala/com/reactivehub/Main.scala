package com.reactivehub

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

case class SayHello()

/**
  * Entry point of the application
  */
object Main extends App {

  println("### Starting Ticket API server ...")

  // Get the host and port from the config file
  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  // Create implicit actor system
  implicit val system = ActorSystem("basic-akka")
  implicit val ec = system.dispatcher

  // Create implicit materializer
  implicit val materializer = ActorMaterializer()

  // Create REST API routes
  val restAPI = new RestAPI(system).routes

  val startFuture = Http().bindAndHandle(restAPI, host, port)
  startFuture.map { serverBinding =>
    println(s"### Server started at http://${host}/${port}")
    serverBinding.localAddress
  }.onFailure {
    case ex: Exception =>
      println(s"### Failed to bind to ${ex.getMessage}")
      system.terminate()
  }
}
