package com.reactivehub

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._

import scala.concurrent.duration._
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

/**
  * REST API endpoints
  */
class RestAPI(system: ActorSystem) {

  import MovieActor._

  implicit val timeout = Timeout(5 seconds)
  implicit val itemFormat = jsonFormat2(Ticket)
  implicit val movieFormat = jsonFormat2(Movie)

  def routes: Route = helloRoute ~ allMoviesRoute ~ movieRoute

  val movieRef = system.actorOf(MovieActor.props, "movie-actor")

  def helloRoute =
    pathPrefix("hello") {
      pathEndOrSingleSlash {
        get {
          complete(OK, "Hello from AKKA")
        }
      }
    }

  def allMoviesRoute =
    pathPrefix("movies") {
      pathEndOrSingleSlash {
        get {
          onSuccess(getMovies()) {
            list => complete(OK, list)
          }
        } ~ post {
          entity(as[Movie]) { movie =>
            addMovie(movie)
            complete(Created)
          }
        }
      }
    }

  def movieRoute =
    pathPrefix("movies" / Segment) {
      name =>
        pathEndOrSingleSlash {
          get {
            onSuccess(getMovie(name)) {
              optional => complete(OK, optional.get)
            }
          } ~ delete {
            deleteMovie(name)
            complete(NoContent)
          }
        }
    }

  def getMovies() = movieRef.ask(GetMovies()).mapTo[Iterable[Movie]]

  def getMovie(name: String) = movieRef.ask(GetMovie(name)).mapTo[Option[Movie]]

  def addMovie(movie: Movie) = movieRef ! AddMovie(movie)

  def deleteMovie(name: String) = movieRef ! DeleteMovie(name)

}
