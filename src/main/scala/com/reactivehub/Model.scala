package com.reactivehub

case class Movie(name: String, tickets: List[Ticket])

case class Ticket(time: String, seat: String)
